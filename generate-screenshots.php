<?php
// header("Content-Type: image/jpeg");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST,OPTIONS,DELETE,PUT');
header("Access-Control-Allow-Headers: X-Requested-With");


$url =  $_GET['url'];
$width = $_GET['width'];
$height = $_GET['height'];

// var_dump(parse_url($url));

$allowedDomains = [  'mobile-test2.map-dynamics.com' , 'mobile.map-dynamics.com', 'mobile.mapd-mobile.test'];


if(  empty($url) || empty($width) || empty($height) || !in_array(parse_url($url)["host"],$allowedDomains) ){
    exit('INVALID DATA');
}


// $url = 'https://mobile.map-dynamics.com/exhibitors.php?Show_ID=4610';
// $width = '1241';
// $height = '234';




$cmd = "node generate-base64.js --url={$url} --width={$width} --height={$height}  2>&1";
// echo $cmd;
$img = shell_exec($cmd);

// echo  "<img src='data:image/jpeg;base64,{$img}' />";    

echo $img;

?>