import yargs from 'yargs';
import { hideBin } from 'yargs/helpers'
const argv = yargs(hideBin(process.argv)).argv;
import captureWebsite from './index.js';

// console.log('a');

(async () => {
	await captureWebsite.base64( argv.url,
	{
		width : argv.width,
        height: argv.height,
        scaleFactor : 1
	}).then((value)=>{
         console.log(value);
    });
})();



