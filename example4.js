import captureWebsite from './index.js';

// Run this file with `$ node example.js`

(async () => {
	await captureWebsite.base64('https://mobile.map-dynamics.com/exhibitors.php?Show_ID=4610','./screenshots/'+ Math.random() + Date.now()+'.png',
	{
		width : 1024,
        height: 1366,
        scaleFactor : 1
	}).then((value)=>{
        console.log(value);
    });
})();
