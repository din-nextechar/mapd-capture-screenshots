import captureWebsite from './index.js';

// Run this file with `$ node example.js`

(async () => {
	await captureWebsite.file('https://www.angelinfra.in/','./screenshots/'+ Math.random() + Date.now()+'.png',
	{
		emulateDevice: 'iPhone X'
	});
})();